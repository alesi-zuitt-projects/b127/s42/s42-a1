/* selecting an element with an id of mainHeading and assigning*/


let heading = document.getElementById('mainHeading');
//Add event listener
/* 1. 1st param is the kind of ebent that it will listen to
2.*/
heading.addEventListener('click', hello = () =>{
	console.log("You clicked me");
	heading.style.color = "Green";
})

//this will turn the color to red
//heading.style.color = "Red";

//this will change the background color
//heading.style.background = "Yellow";